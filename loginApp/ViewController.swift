//
//  ViewController.swift
//  loginApp
//
//  Created by david gestoso lamazares on 3/5/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginStackView: UIStackView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let viewToRound = loginStackView.arrangedSubviews[0]
        let buttonView = loginStackView.arrangedSubviews.last
        
        for view  in loginStackView.arrangedSubviews {
            let control = view as! UIControl
            view.layer.borderWidth = 10
            view.layer.borderColor = UIColor.black.cgColor
        }
        
        if #available(iOS 11.0, *) {
            viewToRound.clipsToBounds = true
            viewToRound.layer.cornerRadius = 30
            viewToRound.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            viewToRound.backgroundColor = .gray
            

        } else {
            // Fallback on earlier versions
        }
        
        buttonView?.backgroundColor = .blue
        buttonView?.tintColor = .white
    }


}



